from proteins import Protein
from time import time

# initialize dict for keeping track of best fold
best_fold = {
    "sequence": "",
    "walk": "",
    "energy": 0
}

# log start time
start_time = time()

# loop through proteins
protein = []
for i in range(5000):

    # set protein an calculate energy
    protein = Protein("PPCHHPPCHPPPPCHHHHCHHPPHHPPPPHHPPHPP", random_walk=True)
    protein.energy()
    # keep track of best fold
    if protein.score < best_fold["energy"]:
        best_fold = {
            "sequence": protein.sequence,
            "walk": ''.join(protein.walk),
            "energy": protein.score
        }

# log end time
runtime = time() - start_time

# print best fold and runtime
print(runtime)
print(best_fold)

# visualize best fold
best_protein = Protein(best_fold["sequence"], best_fold["walk"])
best_protein.energy()
best_protein.draw()

