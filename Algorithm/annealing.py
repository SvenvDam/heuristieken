# Simulated annealing

from proteins import Protein
from time import time
import methods


# set variables
############################################################################

sequence = "HHPHPHPHPHHHHPHPPPHPPPHPPPPHPPPHPPPHPHHHHPHPHPHPHH"

n = len(sequence)
iterations = [20 * n]
rounds = [1000, 50]
choices = ['f', 'l', 'r']
treshold = 0.9
max_rounds = 500

############################################################################

# initialise trackers
found_proteins = []
min_energy = 0

# best folded protein
champion = Protein(sequence, random_walk=True)

# run algorithm
############################################################################

# open txt file for storage of best folds and write sequence
f = open("results.txt", "w")
f.write("Protein sequence: {}\n\n".format(sequence))
f.close()


def update_scores():
    global min_energy
    global champion

    # update minimal energy
    if temp_protein.score <= min_energy:
        min_energy = temp_protein.score

    if temp_protein.score <= champion.score:
        champion = temp_protein

        # log new optimal folds to file
        with open("results.txt", "a") as f:
            f.write("Walk: {}\n".format(champion.walk))
            f.write("Energy: {}\n\n".format(champion.score))

    # show to user
    print("Found energy: {}".format(temp_protein.score))
    print("Cycle lowest energy: {}".format(min_energy))
    print("Global lowest energy: {}\n".format(champion.score))


# log start time
start_time = time()

# keep running algorithm untill user hits ctrl + c
try:
    while True:

        print("Stage 1")
        for i in range(rounds[0]):
            print("1: {}/{}".format(i + 1, rounds[0]))

            # generate random form of protein
            temp_protein = Protein(sequence, random_walk=True)

            # improve fold by annealing
            temp_protein = methods.simulated_annealing(
                temp_protein, iterations[0], choices, 2)

            # strictly improve fold by hillclimber
            temp_protein = methods.hillclimber(
                temp_protein, choices, max_rounds)

            # store result
            found_proteins.append(temp_protein)

            update_scores()

        # get folds with energy of at least ..% of the best fold
        best_proteins = [
            protein for protein in found_proteins if
            protein.score <= treshold * min_energy]

        print("Stage 2")
        for i in range(rounds[1] * len(best_proteins)):
            print("2: {}/{}".format(i + 1, rounds[1] * len(best_proteins)))

            # select one of the best folds
            temp_protein = best_proteins[i % len(best_proteins)]

            # optimize by simulated annealing
            temp_protein = methods.simulated_annealing(
                temp_protein, iterations[0], choices, 1)

            # strictly optimize by hillclimber
            temp_protein = methods.hillclimber(
                temp_protein, choices, max_rounds)

            # store result
            found_proteins.append(temp_protein)

            update_scores()

        # get folds with energy of at least ..% of the (new) best fold
        best_proteins = [
            protein for protein in found_proteins if
            protein.score <= treshold * min_energy]

        print("Stage 3")

        for i, protein in enumerate(best_proteins):

            print("3: {}/{}".format(i + 1, len(best_proteins)))

            # strictly improve most promising folds
            temp_protein = methods.hillclimber(protein, choices, max_rounds)

            update_scores()

        print("Cycle complete\n\n\n")

        # flush generated proteins
        min_energy = 0
        found_proteins = []

# break on user input
except KeyboardInterrupt:
    pass

# log end time
runtime = time() - start_time

############################################################################


# visualize results
############################################################################

# print best fold and runtime
print("\n")
print(runtime)
print("Protein: {}".format(champion.sequence))
print("Walk: {}".format(champion.walk))
print("Energy: {}\n".format(champion.score))

# visualize best fold
# champion.energy()
try:
    champion.draw()
    champion.show()
except:
    pass


############################################################################
