import math
import random


def Metropolis_criterion(e_old, e_new, count, iterations, base):
    '''
    Determine wether or not to accept new form, accounting for 'temperature'
    '''

    if e_new <= e_old:
        return True

    T = base * (0.99 ** (150 * count / iterations))

    criterion_value = math.exp((e_old - e_new) / T)

    return random.random() <= criterion_value
