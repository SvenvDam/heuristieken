from proteins import Protein
import random
from time import time
from proteinlib import set_protein, get_energy


def fress(sequence, walk, max_size, max_iterations):

    length_protein = len(sequence)
    iterations_fress = max_iterations

    # if Protein is too smal return walk
    if length_protein < 4:
        return walk

    # highest node to start partial random walk from
    fress_max = length_protein - 3
    if fress_max >= max_size:
        fress_max = max_size

    # initialize all possible lengths for substring
    fress_choices = list(range(2, fress_max))

    # initialize corresponding weights to sizes
    fress_weights = list(reversed(range(1, fress_choices.__len__() + 1)))

    # might be needed for changing weights
    fress_weights[:] = [x for x in fress_weights]

    counter_fress = 0
    lowest_energy = Protein(sequence, walk).score
    best_walk = walk

    for i in range(iterations_fress):
        # initialize the size of the substring
        size = random.choices(population=fress_choices, weights=fress_weights, k=1)[0]

        # select node to start from
        node = random.randint(0, length_protein - size - 1)

        new_walk = best_walk

        while counter_fress <= (size * 5) and counter_fress <= pow(size, 3):

            # generate random walk of correct size
            sub_walk = ''.join(random.choices("flr", k=size))
            counter_fress += 1

            # check if random substrings fits with beginning and ending of whole string
            if set_protein(sequence, best_walk[:node] + sub_walk + best_walk[(node + size):]) is None:
                if set_protein(sequence, best_walk[:node] + sub_walk + 'r' + best_walk[(node + size + 1):]) is not None:
                    new_walk = best_walk[:node] + sub_walk + 'r' + best_walk[(node + size + 1):]

                elif set_protein(sequence, best_walk[:node] + sub_walk + 'l' + best_walk[(node + size + 1):]) is not None:
                    new_walk = best_walk[:node] + sub_walk + 'l' + best_walk[(node + size + 1):]

                elif set_protein(sequence, best_walk[:node] + sub_walk + 'f' + best_walk[(node + size + 1):]) is not None:
                    new_walk = best_walk[:node] + sub_walk + 'f' + best_walk[(node + size + 1):]

            # check if new walk is self avoiding
            if set_protein(sequence, new_walk) is not None:
                new_protein = Protein(sequence, new_walk)
                new_score = new_protein.score

                # check if new score is lower than lowest score or allows a slightly worse score
                if new_score < (lowest_energy + random.choices(population=[0, 6], weights=[2500, 1], k=1)[0]):
                    protein = Protein(sequence, new_walk)
                    if new_score < lowest_energy:
                        print(new_score, new_walk)
                        best_walk = new_walk
                        return best_walk
                    else:
                        lowest_energy = protein.score
                        best_walk = new_walk

        counter_fress = 0

    # return best fold and runtime
    print((time() - start_time))
    return best_walk


start_time = time()
sequence = "HHPHPHPHPHHHHPHPPPHPPPHPP"
choices = ['f', 'l', 'r']
walk = "fffffffffffffffffffffffff"

new_walk = walk
lowest_score = Protein(sequence, new_walk).score

while True:
    max_iter = random.randint(1, 50)
    max_size = random.randint(3, 20)
    print(' ')
    print(test_max_iter, test_max_size, lowest_score)
    new_walk = fress(sequence=sequence, walk=new_walk, max_size=max_size, max_iterations=max_iter)

    if Protein(sequence, new_walk).score <= lowest_score:
        lowest_score = Protein(sequence, new_walk).score
        with open("results_joost.txt", "a") as f:
            f.write("Walk: {}\n".format(new_walk))
            f.write("Energy: {}\n\n".format(Protein(sequence, new_walk).score))


