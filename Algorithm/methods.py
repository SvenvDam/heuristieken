import annealinglib
import progressbar
import random
from proteins import Protein
import proteinlib


def simulated_annealing(protein, iterations, choices, base):

    # set progress bar in console
    bar = progressbar.ProgressBar(maxval=iterations, widgets=[
        progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()

    # set tracker
    count = 0
    sequence = protein.sequence
    places = range(len(sequence) - 1)

    for __ in range(iterations):

        # choose random node to alter
        place = random.choice(places)

        # retrieve current form of protein
        current_walk = protein.walk

        changes = [x for x in choices if x is not current_walk[place]]

        # alter walk
        new_walk = [
            current_walk[: place] + change + current_walk[place + 1:] for
            change in changes]

        # new_walk.append(flip(current_walk, place))

        new_protein = [
            Protein(sequence, walk) for walk in new_walk
            if proteinlib.set_protein(sequence, walk) is not None]

        if new_protein:
            new_energy = [protein.score for protein in new_protein]

            # energy of current state
            e_old = protein.score

            # energy of candidate
            e_new = min(new_energy)

            index = new_energy.index(e_new)

            # check wether or not to accept new form
            if annealinglib.Metropolis_criterion(
                    e_old, e_new, count, iterations, base):
                protein = new_protein[index]

        # update counter
        count += 1

        bar.update(count)

    bar.finish()

    return protein


def hillclimber(protein, choices, max_rounds):

    count = 0

    sequence = protein.sequence
    places = range(len(sequence) - 1)

    while count < max_rounds:

        place = random.choice(places)

        # retrieve current form of protein
        current_walk = protein.walk

        changes = [x for x in choices if x is not current_walk[place]]

        # alter walk
        new_walk = [
            current_walk[: place] + change + current_walk[place + 1:]
            for change in changes]

        new_protein = [
            Protein(sequence, walk) for walk in new_walk
            if proteinlib.set_protein(sequence, walk) is not None]

        if new_protein:
            new_energy = [protein.score for protein in new_protein]

            # energy of current state
            e_old = protein.score

            # energy of candidate
            e_new = min(new_energy)

            index = new_energy.index(e_new)

            # check wether or not to accept new form
            if e_new <= e_old:
                protein = new_protein[index]

            if e_new < e_old:
                count = 0

        # update counter
        count += 1

    return protein
