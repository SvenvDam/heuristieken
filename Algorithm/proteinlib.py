"""
Protein folding library, takes a sequence and a walk as arguments and turns the protein into a graph
with correct node positions specified by the walk.
Also contains function (get_energy) that draws H-H bonds on a node if possible.
"""

import networkx as nx
import numpy as np


def set_protein(sequence, walk):
    """
    Sets graph nodes and edges according to protein sequence.
    :param sequence:    protein sequence (e.g. 'HHHPHHPHP')
    :param walk:        walk of protein (e.g. 'fflfrfflf'
    :return:            all attributes of graph needed for further analysis of protein (see var 'attrs')
    """

    n = len(sequence)
    nodes = range(n)
    walk = list(walk)

    # get edges from sequence
    edges = [(a, (a + 1)) for a, b in enumerate(list(sequence))][:-1]

    # add nodes and edges to graph and assign weight 1 (regular edges)
    graph = nx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges, weight=1)

    # get labels
    labels = dict(enumerate(list(sequence)))

    # set node colors conditionally to label
    colors = []
    for a, b in enumerate(list(sequence)):
        if b == 'H':
            colors.append(4)
        elif b == 'P':
            colors.append(2)
        elif b == 'C':
            colors.append(1)

    # set position of initial node
    pos = [(0, 0, 0)]

    attrs = {
        'graph': graph,
        'colors': colors,
        'labels': labels,
        'positions': pos,
        'edges': edges
    }

    # needed for checking feasible coordinates
    my_set = set()
    my_set.add(pos[0])

    # set initial direction in lattice (random)
    direction = np.array([[1, 0, 0], [0, 0, 1], [0, 1, 0]])

    # add positions of nodes according to walk
    for i in range(n - 1):
        direction = add_position(pos, i + 1, direction, walk[i], my_set)
        if not np.any(direction):
            break
    if np.any(direction):
        return attrs


def add_position(pos, node, direction, step, my_set):
    """
    Checks which step to take, gets position of next step and adds position.
    :param pos:         list of positions of already added nodes
    :param node:        node to add position of
    :param direction:   direction of current node
    :param step:        f,l,r (forward, left or right)
    :param my_set:      set of infeasible steps (for constructing a self-avoiding walk)
    :return:            new direction of last node of graph
    """
    previous_node = node - 1
    try:
        current_pos = pos[previous_node]
    except IndexError:
        print("Error: not a self-avoiding walk")
        raise

    # temp = take_step(current_pos, step, direction, my_set)
    head = direction[1]
    left = direction[2]
    current = direction[0]
    current_pos = list(current_pos)
    if step == 'd':
        new = [-item for item in head]
        head = current
    elif step == "u":
        new = head
        head = [- item for item in current]
    elif step == "l":
        new = left
        left = -current
    elif step == 'r':
        new = -left
        left = current
    else:
        new = current

    # calculate next position
    next_pos = [x + y for x, y in zip(current_pos, new)]
    next_direction = np.array([new, head, left])

    # get coordinates of current node
    sur = get_surrounding_coordinates(current_pos)

    # initialize the values to check whether move is feasible (self-avoiding)
    feasible_coordinates = []
    values_to_check = [tuple(sur['up']), tuple(sur['right']), tuple(sur['down']), tuple(sur['left']), tuple(sur['back']), tuple(sur['forward'])]

    # if coordinates to check are not already used it will be saved as a feasible coordinate
    for coordinate in values_to_check:
        if coordinate not in my_set:
            feasible_coordinates.append(coordinate)

    # check if next position is in the feasible coordinates
    if tuple(next_pos) in feasible_coordinates:
        my_set.add(tuple(next_pos))
        pos.append(tuple(next_pos))
        return next_direction
    else:
        return []


def get_surrounding_coordinates(position):
    """
    Gets surrounding coordinates of a position.
    :param position:    tuple of coordinates
    :return:            surrounding coordinates
    """

    # get coordinates of current node
    current_x = position[0]
    current_y = position[1]
    current_z = position[2]

    # get coordinates of all surrounding nodes
    check_left = [current_x, current_y + 1, current_z]
    check_forward = [current_x + 1, current_y, current_z]
    check_right = [current_x, current_y - 1, current_z]
    check_back = [current_x - 1, current_y, current_z]
    check_down = [current_x, current_y, current_z - 1]
    check_up = [current_x, current_y, current_z + 1]
    surrounding = {'up': check_up, 'down': check_down, 'left': check_left, 'right': check_right, 'forward': check_forward, 'back': check_back}
    return surrounding


def get_energy(graph, labels, node, pos):
    """
    Takes a node and draws possible H-H bond.
    :param graph:   graph
    :param labels:  dict with labels of nodes (H or P)
    :param node:    node to draw possible bonds of
    :param pos:     list of positions of nodes
    :return:        returns score 0 or 1 depending on whether or not an H-H bond was drawn
    """

    h_indices = [key for key in labels if labels[key] == 'H']
    c_indices = [key for key in labels if labels[key] == 'C']
    score = 0

    current_node = node
    if current_node in h_indices or current_node in c_indices:
        adjacent = list(graph[current_node].keys())
        current_pos = pos[current_node]

        # get coordinates of current node
        sur = get_surrounding_coordinates(current_pos)
        to_check = [sur['up'], sur['right'], sur['down'], sur['left'], sur['forward'], sur['back']]
        neighbors = []
        for item in to_check:
            neighbor = [i for i, tup in list(enumerate(pos)) if tup == tuple(item)]
            if neighbor:
                if (neighbor[0] in h_indices  and current_node in h_indices) and neighbor[0] not in adjacent:
                    neighbors.append(neighbor[0])
                    score = -1
                elif (neighbor[0] in c_indices and current_node in h_indices) and neighbor[0] not in adjacent:
                    neighbors.append(neighbor[0])
                    score = -1
                elif (neighbor[0] in c_indices and current_node in c_indices) and neighbor[0] not in adjacent:
                    neighbors.append(neighbor[0])
                    score = -5

        if neighbors and neighbors[0] in h_indices:
            graph.add_edge(neighbors[0], current_node, weight=0.5)
        elif neighbors and current_node in c_indices and neighbors[0] in c_indices:
            graph.add_edge(neighbors[0], current_node, weight=0.1)
        elif neighbors and neighbors[0] in c_indices:
            graph.add_edge(neighbors[0], current_node, weight=0.5)

    return score
