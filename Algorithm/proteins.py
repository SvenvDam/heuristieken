from proteinlib import set_protein, get_energy
import random
import numpy as np

try:
    from mayavi import mlab
except:
    pass


class Protein:

    def __init__(self, sequence, walk='f', random_walk=False):
        self.sequence = sequence
        self.length = len(sequence)

        if not random_walk:
            if len(walk) < (self.length - 1):
                walk *= (self.length - 1)
        elif random_walk:
            walk = ''.join(random.choices("flrud", k=(self.length-1)))
        self.walk = walk

        self.attributes = set_protein(sequence, walk)
        while not self.attributes:
            self.walk = ''.join(random.choices("flrud", k=(self.length-1)))
            self.attributes = set_protein(sequence, self.walk)

        # calculate energy
        self.score = 0
        graph = self.attributes["graph"]
        labels = self.attributes["labels"]
        positions = self.attributes["positions"]

        for node in graph.nodes():
            self.score += get_energy(graph, labels, node, positions)

    try:
        def draw_bonds(self, bonds, pos, color):
            """
            Draw energy bonds in protein
            :param bonds: list of bonds
            :param pos: scaled position of nodes
            :param color: color for bonds
            """
            graph = self.attributes["graph"]
            all_pos = dict(zip(graph, pos))
            bonds_nodes = [list(item) for item in bonds]
            bonds_pos = [[all_pos[i[0]], all_pos[i[1]]] for i in bonds_nodes]
            for i in range(len(bonds_pos)):
                x1, x2 = bonds_pos[i][0][0], bonds_pos[i][1][0]
                y1, y2 = bonds_pos[i][0][1], bonds_pos[i][1][1]
                z1, z2 = bonds_pos[i][0][2], bonds_pos[i][1][2]
                mlab.plot3d([x1, x2], [y1, y2], [z1, z2], color=color, tube_radius=0.005)

        def draw(self):
            """
            Draws protein according to attributes specified by graph
            """
            graph = self.attributes["graph"]
            pos = self.attributes["positions"]
            colors = self.attributes["colors"]

            # scale positions for drawing and get x, y and z coordinates
            scale = 1 / 3
            scaled_pos = np.multiply(pos, scale)
            xyz = np.array([scaled_pos[v] for v in sorted(graph)])

            # draw graph
            mlab.figure(1, bgcolor=(0, 0, 0))
            mlab.clf()
            pts = mlab.points3d(xyz[:, 0], xyz[:, 1], xyz[:, 2],
                                colors,
                                scale_factor=0.1,
                                scale_mode='none',
                                colormap='RdYlBu',
                                resolution=20)

            # draw covalent bonds
            bonds = [tuple([item[0], item[1]]) for item in list(graph.edges_iter(data='weight')) if item[2] == 1]
            pts.mlab_source.dataset.lines = np.array(bonds)
            pts.mlab_source.update()
            tube = mlab.pipeline.tube(pts, tube_radius=0.01)
            mlab.pipeline.surface(tube, color=(0.8, 0.8, 0.8))

            # draw H-bonds
            h_bonds = [
                tuple([weight[0], weight[1]]) for weight in list(graph.edges_iter(data='weight')) if weight[2] == 0.5
                ]
            self.draw_bonds(h_bonds, scaled_pos, (0, 1, 1))

            # draw C-bonds
            c_bonds = [
                tuple([weight[0], weight[1]]) for weight in list(graph.edges_iter(data='weight')) if weight[2] == 0.1
                ]
            self.draw_bonds(c_bonds, scaled_pos, (1, 0, 1))

            mlab.pipeline.surface(tube, color=(0.8, 0.8, 0.8))
            mlab.show()
    except:
        pass

    def energy(self):
        """
        Draws H-H bonds and calculates energy.
        :return: print of energy level
        """
        graph = self.attributes["graph"]
        labels = self.attributes["labels"]
        positions = self.attributes["positions"]

        for node in graph.nodes():
            self.score += get_energy(graph, labels, node, positions)

        return self.score
