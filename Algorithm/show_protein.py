from proteins import Protein

sequence = "HHPHPHPHPHHHHPHPPPHPPPHPPPPHPPPHPPPHPHHHHPHPHPHPHH"

walk = 'fllflrlfllrrllrfllrflfffflrrfllrffllffrrllrlflfll'


protein = Protein(sequence, walk=walk)

print(protein.score)

print(protein.score)
protein.energy()
protein.draw()
protein.show()


# lrrfrlrfrrlfllrfllrrlrrfllfrllffffrrllrrllflrlfll
# lrrfrlrfrrlfllrfllrrlflrrfrfflrllrlrllrrllflrlfll
# lrrfrlrfrrlfllrfllrrffllrflrflrllrlrllrrllflrlfll

# lrrfrlrfrrlfllrfllrrffllrflrflrllrlrllrrllflrlfll

# -21
# walk = 'lrrfrlrfrrlfllrfllrrlfrfllfrflfrllfrllrrllflrlfll'
