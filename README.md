# Protein Folding

Link to paper:

https://www.sharelatex.com/project/59007a534ebb05ab65c8e805

# Documentation

This documentation contains an explanation of the code used for folding proteins in the HP model.

## Contents
Content | Involved python files | Keywords
------------ | ------------- | -------------
[Getting started](#getting-started) | protein_folding.py | Prerequisites, installation, running algorithms
[The programming of proteins](#the-programming-of-proteins) | proteins.py, proteinlib.py | Visualizing, score function, graph representation
[Random folding algorithm](#random-folding-algorithm) | random_algorithm.py | Random, comparing purposes, champion protein
[Hillclimber](#hillclimber) | hillclimber.py | Random change, no score increase
[Simulated annealing](#simulated-annealing) | annealing.py, annealinglib.py | Metropolis criterion, lower score accepted
[Hillclimber-annealing hybrid](#hillclimber-annealing-hybrid) | hybrid.py, annealinglib.py, methods.py | Switch to annealing, reset protein
[FRSS](#frss) | frss.py | Random subwalk, random length of subwalk
[Genetic](#genetic) | genetic.py, annealinglib.py, methods.py | Hillclimber, annealing, 3 stages, improving chilren
[Breadth first search](#breadth-first-search) | bfs.py | Search entire state space, keep best children, recursive
[Depth first search with pruning](#depth-first-search-with-pruning) | dfs_pruning.py | Search entire state space, pruning condition, recursive
[Authors](#authors) | - | -

## Getting started
This section contains information about the prerequisites, how to install everything and how to run algorithms.

### Prerequisites
Libraries that have a number of other dependencies:
* [Networkx](https://networkx.github.io/documentation/networkx-1.9/install.html) - For the constructing of graphs
* [Mayavi](http://docs.enthought.com/mayavi/mayavi/installation.html) - For the visualizing of these graphs as proteins

Also used some smaller libraries (e.g. numpy, random, json, sys, time, math and progressbar).

N.B. Mayavi is not essential for running algorithms, it is only essential for visualizing the proteins.

### Installing
Download all the files and make sure they are all in the same folder. Install all the requirements:
```
>> pip install -r requirements.txt
```

### Running algorithms
From the python file protein_folding.py each algorithm can be run. First make sure all python files involved with the algorithms are in the same folder as protein_folding.py. Also make sure all log_algorithm.json files are in the same folder. Then run the program from your terminal window:
```
>> python protein_folding.py
```
The program will print all possible sequences and ask you to input a number. Choose a sequence by typing it's associated integer. Next, it will print all possible algorithms and ask you to choose from them, again by inputting an integer. The next step will be choosing a dimension. Choose 0 if you want to fold proteins on a square grid, choose 1 if you want to fold proteins on a cubic grid.
Depending on the algorithm you chose the program will ask you for further input (e.g. iterations or pruning parameters).

## The programming of proteins
**Python files:** *proteins.py, proteinlib.py.*

These files are the backbone of all the algorithms used and rely heavily on the networkx library for folding on a square grid, and the mayavi library for folding on a cubic grid. In this project, proteins are represented by graphs.

### Usage
Import the class Protein in a python file, make an instance of this class with the parameter to specify the sequence (e.g. HHPHHHPHPHHHPH). The other parameters are optional. If none of these are specified the instance will create a protein in a straight line on a square grid. It is possible to specify the walk (e.g. "fllrrllflfrlll"), a boolean determining whether or not a random walk should be generated and a boolean determining the dimension (2d with a square grid or 3d with a cubic grid). From this instance you can get the energy level of the protein with `instancename.score`. One can also return any of the other stored attributes from this instance (see the next section). 

### Proteins.py
In proteins.py the class Protein contains all the info about a protein:
* The sequence (e.g. HHPHHHPHPHHHPH)
* The length (or the number of amino acids, e.g. 14)
* The walk (or the fold, e.g. fllrrllflfrlll)
* The attributes of the protein, which will be discussed later in this section
* The score (or energy level, e.g. -6)

The class contains functions for visualizing the proteins and it's energetic bonds (namely draw() and draw_bonds()) and a function for calculating the energy level of a protein (namely energy()). 

The class will generate a walk that is a straight line on a grid if no walk is specified in the parameters:
```
if len(walk) < (self.length - 1):
    walk *= (self.length - 1)
```
Also, if there is a walk specified, but it is not self-avoiding, it will keep on generating new walks until a self-avoiding one is found:
```
self.attributes = set_protein(sequence, walk)
while not self.attributes:
    self.walk = ''.join(random.choices(walk_choices, k=(self.length-1)))
    self.attributes = set_protein(sequence, self.walk)
```
The function set_protein returns nothing if the walk is not self-avoiding.

#### Visualizing the protein with draw and draw_bonds
With the mayavi library and the two functions draw and draw_bonds the graphs ('proteins') are visualized. 

##### draw (function)
The function draw uses the graph structure, positions of the nodes on the graph and the node colors to draw the protein in a mayavi scene. The normal bonds between nodes are drawn as grey edges and these edges are found by iterating over the edges with weight 1 (for a specification of how these edges got their weight see the next section Proteinlib.py function set_protein and get_energy) and saving their positions. The H and C bonds are found with the function draw_bonds, which will be explained in the next subsection. H-bonds (with a score of -1) are colored blue and C-bonds (with a score of -5) are colored red.

##### draw_bonds (function)
This function takes bonds (namely the edges with weight 0.5 or 0.1) and calculates the starting x, y and z position of the edge and the ending position of each of these bonds and draws a line between them to visualize the bonds. 

N.B. both these functions are in a try except structure because it is not always necessary to visualize the proteins and it takes some time to install all of mayavi's dependencies.

For setting the protein sequences, proteins.py makes use of the functions set_protein and get_energy from the proteinfolding library proteinlib.py, which will be discussed in the following section.

### Proteinlib.py
For programming proteins this library makes use of graphs and sets these up with the networkx library. Each amino acids is a node and between each node is an edge, with weight 1. The weights of edges are used to calculate the energy level of a protein. If an edge has weight 1, no energy is added to the protein. 

#### set_protein (function)
The graph is constructed and for visualizing purposes H nodes are colored blue, P nodes are grey and C nodes are red. The attributes of the graph are saved in a dict:
```
attrs = {
        'graph': graph, # the networkx graph itself
        'colors': colors, # the colors of each node in a list
        'labels': labels, # the label per node (whether it is H, C or P) in a dict
        'positions': pos, # the position of the nodes on the grid in a list
        'edges': edges # the edges of the graph in a list of tuples
    }
```
The function returns these to the Protein class for later usage.

For each amino acid an position on the grid is determined in accordance with the specified walk. This position is set in the function add_position. The `direction` variable keeps track of the 'head', 'nose' and 'left' side of the current amino acid in the grid. This is necessary because of the choice of a walk as a fold (with choices of forward, left, right, up and down), making the direction of the next amino acid relative to the direction of the previous amino acid.

#### add_position (function)
This function looks at the direction of the previous amino acid and places the next amino acid relative to the previous one. 
Example on a cubic grid (applies directly to square grid if choices of `u` and `d` (up and down) are left out).:

If for example the previous amino acid is on position [1, 2, 4] on the grid and has its head in the direction of the positive y-axis ([0, 1, 0]), its left side points to the positive z-axis ([0, 0, 1]) and its face points toward the positive x-axis ([1, 0, 0]). If the next step in the walk string (`step`) is r (right), its new position will be the old position + the new direction which is right (= minus left). So the new position is [1, 2, 4] + (-[0, 0, 1]) = [1, 2, 3]. This way by keeping track of the 'head', 'left side' and 'face' of the protein, the next step is always calculated with minimum effort.

To make sure the walk of a protein is self-avoiding, this function makes use of the other function get_surrounding_coordinates, for reasons the name of the function stipulates, and then checks whether these coordinates are already occupied by other nodes. The occupied coordinates are being tracked in the variable `my_set`. The feasible coordinates are saved and if the next step is a feasible one, the amino acid is placed here and the coordinate is added to `my_set`. 

#### get_surrounding_coordinates (function)
This function takes a position in a grid and returns all surrounding postions in a dict.

#### get_energy (function)
This function takes a node and checks whether there are possible energetic bonds to draw. It does this by using the function get_surrounding_coordinates and using the list of positions of nodes ('pos') to find neighbors. If there are two H nodes neighbors, a score of -1 is added, if a C and an H node are neighbors a score of -1 is also added, if two C amino acids are neighbors, a score of -5 is added. Also for each of these bonds edges are drawn with a weight of 0.5 for bonds involving H and 0.1 for bonds involving C's. This is done for visualizing purposes that were discussed in the section Proteins.py, function draw.

## Random folding algorithm
**Python files:** *random_algorithm.py.*

### Usage
Make sure proteins.py and proteinlib.py are in the same folder as random_algorithm.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of iterations. 

### random_algorithm.py
#### random_algorithm (function)
In this algorithm one can specify a sequence and a number of iterations and it will generate a random fold in each iteration, check it's energy level and if it's lower than the current global energy level, it will save and print this score. 
In the first part of the code the sequence and dimension is specified and from the file log_random.json the current `champion` is retrieved. This is the best protein that this algorithm has found so far (from all of the times it has ever ran). Each iteration the current score is compared to the champion's score and if the current score is lower, the champion protein is replaced. This algorithm is solely for comparing with other algorithms and checking their performance relative to a completely random algorithm.

## Hillclimber
**Python files:** *hillclimber.py*

### Usage
Make sure proteins.py and proteinlib.py are in the same folder as hillclimber.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of iterations.

### hillclimber.py
#### hillclimber_algorithm (function)
This algorithm takes a random place in the sequence and raplaces it with the choices of steps that are not already on that place in the walk. So if the randomly chosen place in a sequence is 2 and on place 2 in the current walk is a step of `l` (left), this algorithm will try step `r` and `f` (and `u` and `d` on a cubic grid). Before proceeding the algorithm will check if the generated walk is self-avoiding with this line of code:
```
if proteinlib.set_protein(sequence, walk) is not None
```
This method will be used in every algorithm to check whether or not a walk is self-avoiding. If this is the case, it will then proceed to check the new energy levels for each of these changes, and accept the change if the energy level is lower with the change, than without. This process will keep on going for a predetermined amount of iterations.

#### hillclimber (function)
This function initializes the hillclimber algorithm, sets the possible steps to be taken (`choices`) and prints the results for the user after the hillclimber is done. If the user has installed mayavi the best protein will be visualized.

## Simulated annealing
**Python files:** *annealing.py, annealinglib.py*

### Usage 
Make sure proteins.py and proteinlib.py are in the same folder as annealinglib.py and annealing.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of iterations.

### Annealing.py
#### annealing_algorithm (function)
This function contains the actual simulated annealing algorithm. The rest of the code is similar to the hillclimber algorithm (see [hillclimber algorithm](#hillclimberpy)), only now instead of only accepting lower energy levels, the Metropolis criterion in annealinglib.py is checked to see if the algorithm will accept the new fold. 

#### annealing (function)
Function for setting variables and initializing simulated annealing. The code starts by setting some variables. The choices of steps to make depend on the dimension. The rest is simular to the hillclimber function (see [hillclimber](#hillclimber_algorithm-function)).

#### Annealinglib.py, Metropolis_criterion (function)
This function accepts the new protein immediately if the new energy level is lower than the old level. If this is not the case, it calculates a criterion value for the new protein to pass. A random number is generated, and if this number is smaller than the criterion value, the new protein with a lower score will be accepted. If the random number is larger than the criterion value, the new protein will not be accepted.

## Hillclimber-annealing hybrid
**Python files:** *hybrid.py, annealinglib.py, methods.py*

### Usage 
Make sure proteins.py, proteinlib.py, annealinglib.py and methods.py are in the same folder as hybrid.py, run normally (see [running algorithms](#running-algorithms)).

### hybrid.py
#### hybrid_algorithm (function)
In this hybrid, a new variable `flaw_count` is introduced, that keeps track of the number of iterations without finding a better fold (with a lower energy level). If the flaw count is larger than the variable `max_rounds`, the hillclimber switches to an annealing algorithm. Also, the variable `switch_count` is introduces, which keeps track of how many times the algorithm switches from a hillclimber to simulated annealing. Before switching to simulated annealing, if the flaw count is larger than the max rounds, the variable `switch_count` is checked and if it is larger than the variable `max_switches` the protein resets and the algorithm starts over with a new protein.

#### hybrid (function)
This code is again similar to the hillclimber algorithm and the annealing algorithm (see [hillclimber algorithm](#hillclimberpy) and [simulated annealing](#annealingpy)). 

### methods.py
This file contains a function for a simulated annealing algorithm (namely `simulated_annealing(protein, iterations, choices, base)`) and for a hillclimber algorithm (namely `hillclimber(protein, choices, max_rounds)`). These functions can be imported into other files and are used in the hybrid algorithm. For explanation of the code within these functions see [hillclimber function](#hillclimber_algorithm-function) and [simulated annealing function](#annealing_algorithm-function).

## FRSS
**Python files:** *frss.py*

### Usage
Make sure proteins.py and proteinlib.py are in the same folder as frss.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of iterations and a number of iterations without improving after which a new random walk is inserted.

### frss.py
#### frss (function)
This function generates a new walk with the FRSS method, by changing a part of the walk (the subwalk). First some variables are set, the most important of which are `fress_choices` and `fress_weights`:
```
fress_choices = list(range(2, fress_max))
fress_weights = list(reversed(range(1, fress_choices.__len__() + 1)))
```
The choices for fress is a list of integers that determines how long the subwalk will be that will be changed in the current walk. The weights are inversely related to the length of the substring; the smaller a substring, the larger the weight, and the bigger the chance this length will be chosen for the substring. This means the size of a substring is chosen with the following line of code:
```
size = random.choices(population=fress_choices, weights=fress_weights, k=1)[0]
```
A random part of the current walk is chosen to start from and called `node`. A maximum number of iterations is chosen depending on the dimension and on `size`. In each iteration a random walk with the aforementioned size is generated. It is then checked of this walk fits, and if it does the new protein is accepted if the new score is lower than the lowest energy plus a small random margin:
```
new_score < (lowest_energy + random.choices(population=[0, total_slack], weights=[1750, 1], k=1)[0])
```
If the subwalk does not fit, all possible choices are tried at the first step right after the subwalk, and if it still does not fit, the algorith moves on to the next iteration.

#### run_frss (function)
Function for intializing FRSS. Sets some variables and calls `frss` for a fixed number of times (`max_while`). All of this goes on for a fixed number of iterations, and if there has not been found a better score for a predetermined amount of iterations (`iwi`) a new random protein is generated for the next iteration.

## Genetic
**Python files:** *genetic.py, annealinglib.py, methods.py*

### Usage
Make sure proteins.py, proteinlib.py and methods.py are in the same folder as genetic.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of proteins to generate per round.

### genetic.py
#### update_scores (function)
This function updates (if necessary) the lowest found score per cycle `min_energy`, the global lowest score `champion.score` and the current score `temp_protein.score`.

#### genetic (function)
This algorithm runs until the user hits ctrl + c in the terminal window. In stage 1 a protein with a random fold is generated, it is then improved by simulated annealing for a number of times dependent on the length of the sequence, namely:
```
n = len(sequence)
iterations = 30 * n
```
After this, the protein is improved by a hillclimber untill no better fold is found for 1000 iterations. The result is stored and the scores are updated with the function update_scores (see [update_scores](#update_scores-function)). A list of the best proteins is kept, where proteins are only kept if they meet the following condition:
```
protein.score <= threshold * min_energy
```
With `min_energy` being the lowest found energy of the previous cycle. Stage 1 is repeated for a fixed number of iterations. Then stage 2 starts. This stage is repeated a fixed number of times per protein in the list of the best proteins from stage 1. The rest of this stage is the same as stage 1. Again the most promising proteins are kept and in stage 3 these are finally improved with a hillclimber.

## Breadth first search
**Python files:** *bfs.py*

### Usage
Make sure proteins.py, proteinlib.py and methods.py are in the same folder as bfs.py, run normally (see [running algorithms](#running-algorithms)). For this algorithm the program will ask you to specify a number of random proteins to generate for the starting pool, a number of parents and an amount of proteins you would like to see returned when the algorithm finishes.

### Breadth first search algorithm
For this algorithm, some variables are set; `starting_iter` and `parents_size`. These specify the amount of random walks generated in the first step and the amount of parents that are kept from these random walks. 

#### bfs
This function starts with a straight walk for the protein. It then chooses the node up until which a random walk will be generated:
```
 node = math.floor(len(sequence) / 3)
```
Which is up until one thirds of the sequence. Then an amount of random walks to start with are generated and their scores are calculated. The duplicates are deleted and therefore if the amount of parents is larger than the amount of generated random walks, this is adjusted for.
```
if parents_size > len(starting_pool):
    parents_size = len(starting_pool)
```
The best parents are chosen (with the lowest score) and the next fase is initialized.

#### bfsauxiliary
This function will keep adding steps to the walks generated by `bfs` until the walk is sufficiently long.
```
if len(sequence) == (len(parents[0]['walk']) + 1):
    return parents
```
If the walk is not sufficiently long yet, it will try each possible step on each parent, save the scores (if the walks are self-avoiding) and keep the best of these children in an array:
```
for j in range(loop_range):
    parents.append(children[sorted_indices[j]])
```
This will recursively continue until the walk is sufficiently long.

Finally, the algorithm prints the 5 best scores and saves the best score if it is better than the current champion score.

## Depth first search with pruning
**Python files:** *dfs_pruning.py*

**Extra JSON file:** *prune_parameters.json*

### Usage
Make sure proteins.py, proteinlib.py and prune_parameters.json are in the same folder as dfs_pruning.py, run normally (see [running algorithms](#running-algorithms)). This algorithm will not ask you to specify the pruning parameters, simply because there are too many and the way they get selected is in a trial and error stage. If the user wants to change these parameters, they can be changed within the prune_parameters.json file.

### dfs_pruning.py
#### dfs (function)
First some parameters are set; `h_threshold` being the number of H amino acids that have to be present in a sequence before the algorithm will start pruning, `score_threshold`, being the threshold of the current best score that the algorithm has to be at before it will start pruning (variable, can be chosen when running this algorithm), then a list of thresholds (`thresholds`) for the non-linear pruning function. These represent the threshold that will have to be met before a pruning condition will grow larger. Each time the best global score of a protein exceeds one of these thresholds, the pruning condition will increase. `c_threshold` is the number of C amino acids a sequence has to have before an extra amount will be added to the pruning condition and finally `prune_pars`, a list of parameters that determine what amount will be added to the pruning function after a certain threshold is exceeded.

#### pruned (function)
In this function the pruning condition will be set depending on all the parameters determined earlier, this condition will be compared to a score and if the condition is met, the function will return `False`, and if it is not met it will return `True`.

#### depth_first_search_3d (function)
This is a recursive depth first search algorithm that takes a walk, checks if the protein made with this walk has a lower energy level than the global lowest energy level (namely `best_score`) and prints it if this is the case. It then proceeds to the next walk in the tree, if the pruning condition is met (so if `pruned(sequence, protein.score)` returns `False`). If the condition is not met, all longer walks from this subwalk on are pruned, never to be looked at again. This algorithm theoretically searches the entire state space of a protein by appending a step (e.g. `'r'`) until the subwalk has the maximum length (namely the length of the sequence minus 1), then pops the last added step and adds another step (e.g. `'l'`). This continues until all the possible walks have been generated or pruned. 

#### depth_first_search (function)
This is the same algorithm as the last one only now without the possible steps `'u'` and `'d'`, making it fit for the two-dimensional problem. The reason these are in two seperate functions is speed.


## Authors
* **Sven van Dam** - *10529772*
* **Joost Kooijman** - *10760768*
* **Laura Ruis** - *10158006* 

With guidance from tech advisor Cindy Allis and Bas Terwijn.
